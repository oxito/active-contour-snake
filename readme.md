# Readme.md

## Segmentação de Imagens e Avaliação

Este notebook Jupyter contém um script em Python para a segmentação de imagens usando contornos ativos e filtragem gaussiana. Além disso, inclui métricas de avaliação, como Interseção sobre União (IoU) e precisão de pixels, para avaliar a qualidade dos resultados da segmentação.

### Dependências

Certifique-se de ter as seguintes bibliotecas Python instaladas:

```bash
pip install numpy matplotlib scikit-image opencv-python pandas scipy
```

### Segmentação de Imagens

A função **segment_image** recebe o caminho de um arquivo de imagem, um rótulo e um contorno final opcional como entrada. Ela realiza a segmentação da imagem usando contornos ativos e salva a imagem segmentada no diretório "dataset/processed/".

### Métricas de Avaliação
O script calcula duas métricas de avaliação:

**Interseção sobre União (IoU)**: Mede a sobreposição entre a máscara verdadeira e a imagem segmentada. A função calc_iou calcula o IoU para cada imagem no diretório "cars/masks/" e imprime a média do IoU.

**Precisão de Pixels**: Mede a porcentagem de pixels segmentados corretamente em comparação com o número total de pixels. A função calc_pixel calcula a precisão de pixels para cada imagem e imprime a média da precisão de pixels.

### Visualização de Exemplo
O script também inclui uma visualização de exemplo de uma máscara verdadeira selecionada aleatoriamente e sua imagem segmentada correspondente. Os resultados são exibidos lado a lado para comparação.

### Resultados
Os resultados das métricas de avaliação são impressos no console. A Interseção sobre União e a precisão de pixels fornecem insights sobre o desempenho do algoritmo de segmentação de imagens.

### dataset
O dataset utilizado encontra-se no diretório carros desse projeto e contém 20 imagens